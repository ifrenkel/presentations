PackageMetadata Sync
---

<!-- pause -->

## Problem to solve

Mechanism by which a GitLab instance fetches the latest package metadata from the `license-db` infrastructure. Package metadata is info about Open Source packages and security advisories. This data is instance-wide (e.g. there are no per project data).

<!-- pause -->

## Why not use an api?

* Some instances are network-limited and cannot access an api as a data source.
* Data interface instead of an api interface has some benefits.
* Instances don't need to sync all package registry data (e.g. `maven` projects only).

<!-- pause -->

## Package Metadata Sync solution

* Export package metadata as to a publicly accessibly location via a simple format.
* Allow GitLab instances to customize how they sync this data.
* Use progressive data protocol to limit data transfer over the network and data inserted into the database.
* Use straight forward data format to simplify synchronization and database updates.

<!-- end_slide -->

PackageMetadata Sync
---

## Components in this system

- `license-db` infrastructure.
- GitLab instances.
- The data at rest.

<!-- pause -->

license-db
---

## Fetch and classify 

![](feeder.mmd.png)

<!-- pause -->

## Export

* Enforces sync protocol for data at rest.
* Progressive updates to decrease update size.
* Compresses license data.

<!-- pause -->

![](exporter.mmd.png)

<!-- end_slide -->

Data at rest
---

* In gcp bucket by default.
* Updates stored incrementally as sequenced prefixes (timestamps).

<!-- pause -->

## Directory structure

```
gs://prod-export-license-bucket-1a6c642fc4de57d4/v2/cargo
├── 1712946196
│   ├── 000000000.ndjson
│   ├── 000000001.ndjson
│   ├── 000000002.ndjson
│   ├── 000000003.ndjson
│   ├── 000000004.ndjson
│   ├── 000000005.ndjson
│   ├── 000000006.ndjson
│   └── 000000007.ndjson
├── 1715181024
│   └── 000000000.ndjson
├── 1716217570
│   └── 000000000.ndjson
└── 1716822403
    └── 000000000.ndjson
```

<!-- pause -->

## Data

```json
{"name":"steel-server","lowest_version":"0.1.0","highest_version":"0.1.0","default_licenses":["MIT"]}
{"name":"unarj-rs","lowest_version":"0.1.0","highest_version":"0.1.0","default_licenses":["MIT"]}
{"name":"cedar-java-ffi","lowest_version":"3.1.0","highest_version":"3.1.0","default_licenses":["Apache-2.0"]}
{"name":"f3l_features","lowest_version":"0.2.0","highest_version":"0.2.0","default_licenses":["Apache-2.0"]}
{"name":"string-literals","lowest_version":"1.0.0","highest_version":"1.0.0","default_licenses":["Apache-2.0"]}
{"name":"wasm_bindgen_cfg","lowest_version":"0.2.0","highest_version":"0.2.0","default_licenses":["unknown"]}
```

<!-- end_slide -->

GitLab instance
---

* 2 supported connection types.
    * Sync by connecting from bucket directly.
    * Sync from the same structure on disk.

<!-- pause -->

![](gl-instance-high-level.mmd.png)

<!-- pause -->


## Sync triggered

![](trigger-sync.mmd.png)

<!-- pause -->

## Sync executed

![](sync-execute.mmd.png)

<!-- end_slide -->

SyncService
---

## Run

* Find last checkpoint.
* Get correct connector.
* Fetch all data since last checkpoint and ingest.


```lua {1|2|4|6-9|all} +line_numbers
def execute(sync_config={ storage_type:'gcp' }, data_type='licenses', purl_type='cargo')
  checkpoint = Checkpoint.where(data_type: data_type, purl_type: purl_type)

  connector = Connector.for(sync_config.storage_type)

  connector.data_after(checkpoint).each do |ndjson_chunk|
    data = parse(ndjson_chunk)
    ingest(data)
  end
end
```

<!-- pause -->

## Connector

```lua +line_numbers
  connection ||= Google::Cloud::Storage.anonymous
  bucket = connection.bucket(bucket_name)
  bucket.files(prefix: file_prefix).all.lazy.map do |file|
    io = file.download
    yield io
  end

```

<!-- pause -->

## Ingestion

SyncService is the generic component and decides on the type of ingestion based on the `data_type` supplied.

```lua +line_numbers
      if sync_config.advisories?
        PackageMetadata::Ingestion::Advisory::IngestionService.execute(data)
      else sync_config.v2?
        PackageMetadata::Ingestion::CompressedPackage::IngestionService.execute(data)
      end
```

<!-- end_slide -->

Ingestion generally follows a batched task per model pattern:

```
tree ee/app/services/package_metadata/ingestion

ee/app/services/package_metadata/ingestion
├── advisory
│   ├── advisory_ingestion_task.rb
│   ├── affected_package_ingestion_task.rb
│   └── ingestion_service.rb
├── compressed_package
│   ├── ingestion_service.rb
│   ├── license_ingestion_task.rb
│   └── package_ingestion_task.rb
├── data_map.rb
├── ingestion_service.rb
└── tasks
    ├── base.rb
    ├── ingest_licenses.rb
    ├── ingest_package_version_licenses.rb
    ├── ingest_package_versions.rb
    └── ingest_packages.rb

4 directories, 13 files
```

<!-- pause -->

Models need to include the `BulkInsertSafe` concern.

```lua +line_numbers

module PackageMetadata
  class Package < ApplicationRecord
    include BulkInsertSafe
   end
end
```

```lua +line_numbers
module PackageMetadata
    module Ingestion
        module CompressedPackage
            class LicenseIngestionTask
              ApplicationRecord.transaction do
                PackageMetadata::License.bulk_upsert!(license_data, unique_by: ['spdx_identifier'])
              end
        end
    end
end
```

<!-- end_slide -->

Conclusion
---

This generally works but has a few drawbacks:
* For advisories, the *pull* nature of sync adds several parts with a potential for scan latency.
* Data sizes are quite large and compression needs to be improved.
* The sync system is still pretty complicated and can do with simplification.
