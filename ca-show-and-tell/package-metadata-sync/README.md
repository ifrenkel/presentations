# PackageMetadata Sync presentation

This containst he `preseterm` demo for PackageMetadata sync.

## Requires

[presenterm](https://github.com/mfontanini/presenterm)


## How to run

In terminal: `presenterm presentation.md`

## How diagrams were generated

There are several `mermaid` (`.mmd`) files for the graphs in this presentation. These were generated in the following way:

`docker run --rm -v $(pwd):/data minlag/mermaid-cli -i $diagram.mmd -o $diagram.mmd.png  -t forest  -s 2`

Note: not all terminals support png images (for example, `alacritty` does not).

## Other

[Recorded video of presentation](https://youtu.be/e48Zgl_9_x4)

